# Enhancing Earth System Model Evaluation with Data Cube Enabled Machine Learning

## Introduction
Machine learning (ML) techniques offer significant potential to enhance climate model evaluations, providing a deeper understanding of Earth system processes. However, applying ML to climate data with high spatial and temporal frequency can be technically challenging, particularly due to the large data volumes that often exceed the memory capacities of computing systems. 

## Objectives
The primary objectives of this pilot project include:

1. **Integrate Cloud-Based Data with ESMValTool**: Enhance the Earth System Model Evaluation Tool (ESMValTool) by interfacing it with cloud-based Earth system data cubes to improve model evaluation workflows.
2. **Implement ML-Based Analysis**: Develop an ML-based analysis package for ESMValTool, focusing on causal discovery applied to climate data, particularly Arctic-midlatitude teleconnections.
3. **Improve Memory Efficiency in Data Processing**: Leverage cloud data to reduce memory usage during data processing, allowing the efficient application of ML techniques on large datasets.

## Key Features
- **ESMValTool Extension**: ESMValTool was enhanced to interface with Earth system data cubes, enabling cloud-based data analysis through a pre-processing infrastructure known as "CMORizer scripts." These scripts convert data from cloud sources into the CMOR format required by ESMValTool.
- **ML Integration**: An ML-based analysis package, specifically focusing on causal discovery algorithms like PCMCI, was integrated into ESMValTool for evaluating climate models' performance in understanding Arctic-midlatitude teleconnections.
- **Cloud Data Utilization**: By processing cloud-ready Zarr datasets, this project demonstrated the potential of using cloud-stored data for climate model evaluation, improving data handling efficiency and enabling more comprehensive analyses.

## Outcomes
The pilot project delivered the following outcomes:

1. **Extended ESMValTool for Cloud-Based Data**: The tool can now access and process cloud-based data cubes using CMORizer scripts, which convert datasets to CMOR standards directly from the cloud, eliminating the need to download large datasets.
2. **ML-Based Climate Model Evaluation**: The integration of causal discovery techniques, particularly PCMCI, facilitated the analysis of Arctic-midlatitude teleconnections and the impacts of Arctic Amplification on midlatitude weather.
3. **Improved Data Handling**: By leveraging cloud data, the project enabled memory-efficient data handling for ML applications, significantly improving the scalability of climate model evaluations.

## Challenges and Gaps
Some challenges were encountered during the project, including:

1. **Lack of Full Cloud Support in Iris Library**: The Iris library, used by ESMValTool for data processing, currently lacks support for Zarr datasets, requiring a temporary workaround using the xarray library.
2. **License Incompatibility**: Due to licensing issues, the ML package (Tigramite) could not be fully integrated into ESMValTool. Instead, a workaround was implemented to preprocess data in ESMValTool and use it with external ML packages.

## Future Directions
To fully realize the potential of cloud-based data integration in ESMValTool, future work will focus on:

1. **Enhancing Cloud Support**: The ESMValTool community is actively working on full cloud support, particularly improving compatibility between the Iris and xarray libraries for Zarr datasets.
2. **Expanding ML Applications**: Further work will explore the integration of additional ML-based diagnostic packages and expand scientific use cases, improving the tool's applicability to a wider range of Earth system science problems.
3. **Optimizing Performance for Large Datasets**: Future development will focus on improving the memory footprint and processing speed of ESMValTool for high-resolution climate models.

## Relevance for the Community
This pilot project provides a crucial stepping stone for using cloud-based data and ML techniques in Earth system science. It is particularly relevant to climate model developers, the climate informatics community, and high-performance computing centers. The outcomes of this project align with the FAIR (Findable, Accessible, Interoperable, Reusable) principles and support the NFDI4Earth community by enabling more efficient, scalable, and reusable workflows for climate model evaluation.

## Funding
This work has been funded by the German Research Foundation (NFDI4Earth, DFG project no. 460036893, [https://www.nfdi4earth.de/](https://www.nfdi4earth.de/)).
